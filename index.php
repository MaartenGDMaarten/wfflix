<?php
session_start();

/* Composer - Loads all classes, so they can be universally accessed */
require 'vendor/autoload.php';

/* Makes the feature View accessible in all Controllers */
require 'core/support.php';

/* Prepare the (re)usable database connection class */
App::bind('config', require 'config.php');
App::bind('query', Connection::make(App::get('config')));

/* Anyting that has to do with routes goes through here */
Router::load('routes.php')->direct(Request::uri(), Request::method()); //chain methods


