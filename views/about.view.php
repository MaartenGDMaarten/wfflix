<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" href="/views/css/about.css">

    <!-- Other Head Items -->
    <link rel="icon" type="image/x-icon" href="/images/logo-header-home.webp"> <!-- IMG in browser tab -->
    <title>Over Ons</title>
</head>
<body>
<!-- Navigation -->
<?php
require 'utils/navigation.php';
require 'views/utils/LoginCheck.php';
?>
<!-- End Navigation -->

<section>
    <!---About us intro  ---->
    <div class="about-us">
    </div>
    <!---About us intro End ---->
</section>
<section>
    <!---Our Story ---->
    <div class="container mt-5">
        <div class="row">
            <div class="col-sm-12">
                <h3>Our Story</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis commodo tortor ut nisi tempor,
                    non dignissim massa consectetur. Sed vitae tellus a lectus lobortis pellentesque.
                    In sed pharetra eros. Etiam sodales luctus pretium. Morbi fringilla posuere nulla ut luctus.
                    Vestibulum feugiat odio non nisl iaculis, ac blandit nisi placerat. Sed eu porta erat.
                    Maecenas pretium euismod sapien vitae auctor. Aenean a nisi at erat efficitur tincidunt a ac
                    nibh.</p>
                <br>
            </div>
            <!---Our Story End ---->
</section>
<section>
    <!---Our Employees ---->
    <div class="container mt-5">
        <h3> Our Employees</h3>
        <div class="row justify-content-center">
            <div class="col-sm-3">
                <div class="card" style="width: 200px">
                    <img class="card-img-top" src="/images/Avatar.jpg" alt="Card image" style="width: 100%">
                    <div class="card-body">
                        <h5 class="card-title">Tobie</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional
                            content.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card" style="width: 200px">
                    <img class="card-img-top" src="/images/Avatar.jpg" alt="Card image" style="width: 100%">
                    <div class="card-body">
                        <h5 class="card-title">Faissal</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional
                            content.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card" style="width: 200px">
                    <img class="card-img-top" src="/images/Avatar.jpg" alt="Card image" style="width: 100%">
                    <div class="card-body">
                        <h5 class="card-title">Maarten</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional
                            content.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card" style="width: 200px">
                    <img class="card-img-top" src="/images/Avatar.jpg" alt="Card image" style="width: 100%">
                    <div class="card-body">
                        <h5 class="card-title">Jeffrey</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional
                            content.</p>
                    </div>
                </div>
                <br>
            </div>

            <!---Our Empoloyees End ---->
</section>
<section>
    <!---Our Proces ---->

    <div class="container mt-5">
        <div class="row">
            <div class="col-sm-8">
                <h3>Our Proces</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis commodo tortor ut nisi
                    tempor,
                    non dignissim massa consectetur. Sed vitae tellus a lectus lobortis pellentesque.
                    In sed pharetra eros. Etiam sodales luctus pretium. Morbi fringilla posuere nulla ut
                    luctus.
                    Vestibulum feugiat odio non nisl iaculis, ac blandit nisi placerat. Sed eu porta erat.
                    Maecenas pretium euismod sapien vitae auctor. Aenean a nisi at erat efficitur tincidunt
                    a ac
                    nibh.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis commodo tortor ut
                    nisi tempor,
                    non dignissim massa consectetur. Sed vitae tellus a lectus lobortis pellentesque.
                    In sed pharetra eros. Etiam sodales luctus pretium. Morbi fringilla posuere nulla ut
                    luctus.
                    Vestibulum feugiat odio non nisl iaculis, ac blandit nisi placerat. Sed eu porta erat.
                    Maecenas pretium euismod sapien vitae auctor. Aenean a nisi at erat efficitur tincidunt
                    a ac
                    nibh.</p>
            </div>
            <div class="col-sm-4">
                <img class="card-img-top" src="/images/computer.png" alt="Card image" style="width: 100%">


            </div>
        </div>
    </div>

</section>
<!---Our Proces End ---->
<!---Our Products---->
<div class="container mt-5">
    <div class="row">
        <div class="col-sm-4">
            <img class="card-img-top" src="/images/learning.webp" alt="Card image"
                 style="width: 100%">

        </div>
        <div class="col-sm-8">
            <h3>Our Products</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis commodo tortor ut nisi
                tempor,
                non dignissim massa consectetur. Sed vitae tellus a lectus lobortis pellentesque.
                In sed pharetra eros. Etiam sodales luctus pretium. Morbi fringilla posuere nulla ut
                luctus.
                Vestibulum feugiat odio non nisl iaculis, ac blandit nisi placerat. Sed eu porta erat.
                Maecenas pretium euismod sapien vitae auctor. Aenean a nisi at erat efficitur tincidunt
                a ac
                nibh.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis commodo tortor ut
                nisi tempor,
                non dignissim massa consectetur. Sed vitae tellus a lectus lobortis pellentesque.
                In sed pharetra eros. Etiam sodales luctus pretium. Morbi fringilla posuere nulla ut
                luctus.
                Vestibulum feugiat odio non nisl iaculis, ac blandit nisi placerat. Sed eu porta erat.
                Maecenas pretium euismod sapien vitae auctor. Aenean a nisi at erat efficitur tincidunt
                a ac
                nibh.
                <button type="button" onclick="alert('Hello world!')">Click Me!</button>
            </p>
        </div>
        <!---Our Products End ---->

        <!-- Footer -->
        <?php require 'utils/footer.php' ?>
        <!-- End Footer -->

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
                integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
                crossorigin="anonymous"></script>

</body>
</html>