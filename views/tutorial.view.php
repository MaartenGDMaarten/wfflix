<!doctype html>

<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
        <link rel="stylesheet" href="views/css/tutorial_styles.css">
        <link rel="icon" type="image/x-icon" href="/images/logo-header-home.webp"> <!-- IMG in browser tab -->
        <title>Tutorials</title>

    </head>

    <body>
        <!-- navbar + drop down stuff -->
        <!-- <?php require 'views/utils/navigation.php'; ?> -->

      <!-- show case text-->
      <section class="bg-dark text-light p-1 text-center">
        <div class="container">
            <div class="div d-sm-flex my-3 align-items-center justify-content-between p-5">
                <div>
                    <h1>Welcome <span class="text-warning">Guest</span> to wefflix!</h1>
                </div>
            </div>
        </div>
    </section>




        <!-- light grey bar (quick info) + optinal sertch function -->
        <section class="section bg-secondary text-light p-5">
            <div class="container">
                <div class="d-md-flex justify-content-between align-items-center">
                    <h3 class="mb-3 mb-md-0">Why should you make a account here?</h3>
                </div>
            </div>
        </section>

        <!-- Frequently Asked Questions accordion -->
        <section id="questions" class="p-5">
            <div class="container">
                <div class="accordion" id="accordion">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="heading1">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
                               What is wefflix
                            </button>
                        </h2>
                        <div id="collapse1" class="accordion-collapse collapse" aria-labelledby="heading1" data-bs-parent="#accordion">
                            <div class="accordion-body">
                                - Wefflix is a cobination between netflix, code academy and W3school, you have online cources which you can follow to learn één or more programs/proggraming laugueses
                            </div>
                        </div>
                    </div>
                </div>
            <div class="accordion" id="accordion">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="heading2">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                           Is it free
                        </button>
                    </h2>
                    <div id="collapse2" class="accordion-collapse collapse" aria-labelledby="heading2" data-bs-parent="#accordion">
                        <div class="accordion-body">
                            YES, Right now it's 100% free to use either as creator or as viewer!
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </section>

        <section class="section bg-secondary text-light p-5">
            <div class="container">
                <div class="d-md-flex justify-content-between align-items-center">
                    <h3 class="mb-3 mb-md-0">Why Wefflix</h3>
                </div>
            </div>
        </section>
        <section class="bg-secondary text-light">
            <div class="container">
                <div class="col-6">
                    Wefflix is the 1 shop/stasion for all your proggraming need, cources for every leangues,
                    from people for people! the youtube of coding as we like to call it
                </div>    
            </div>

        </section>

        <section class="section bg-secondary text-light p-5">
            <div class="container">
                <div class="d-md-flex justify-content-between align-items-center">
                    <h3 class="mb-3 mb-md-0">Tutorial for our site</h3>
                </div>
            </div>
        </section>

        <Section class="bg-secondary text-light">
            <div class="container">

                <!-- login easy staps -->
                <div class="col-6">
                    <H3>Step 1</H3>
                    <br>
                    In the picture below it shows you where to log in and sign up
                </div> 
                <div class="col-6">
                    <br>
                    <img src="images/tutorial/logintuto.png" class="d-block w-100 video-foto-mainpage" alt="...">
                </div>
                <div class="col-6">
                    <br>
                    Here you will find what you will see if you're logged in:
                </div>
                <div class="col-6">
                    <br>
                    <img src="images/tutorial/profile tuto.png" class="d-block w-100 video-foto-mainpage" alt="...">
                </div>
                <div class="col-6">
                    <br>
                    Forgot you password, no worrry we have a password reset option as well
                </div>
                <div class="col-6">
                    <br>
                    <img src="images/tutorial/profilepwreset.png" class="d-block w-100 video-foto-mainpage" alt="...">
                </div>

                <!-- site navigation -->
                <!-- nav to overview -->
                <div class="col-6">
                    <H3>Step 2</H3>
                    <br>
                    navigate to "overview"
                </div>
                <div class="col-6">
                    <br>
                    <img src="images/tutorial/overview1.png" class="d-block w-100 video-foto-mainpage" alt="...">
                </div>
                <div class="col-6">
                    <br>
                    You will no see something lik this:
                </div>
                <div class="col-6">
                    <br>
                    <img src="images/tutorial/overview2.png" class="d-block w-100 video-foto-mainpage" alt="...">
                </div>
                <!-- nav to overons -->
                <div class="col-6">
                    <br>
                    navigate to "over ons"
                </div>
                <div class="col-6">
                    <br>
                    <img src="images/tutorial/overons1.png" class="d-block w-100 video-foto-mainpage" alt="...">
                </div>
                <div class="col-6">
                    <br>
                    You will no see something lik this:
                </div>
                <div class="col-6">
                    <br>
                    <img src="images/tutorial/overons2.png" class="d-block w-100 video-foto-mainpage" alt="...">
                </div>

                <!-- nav to contact -->
                <div class="col-6">
                    <br>
                    navigate to "Contact"
                </div>
                <div class="col-6">
                    <br>
                    <img src="images/tutorial/contact1.png" class="d-block w-100 video-foto-mainpage" alt="...">
                </div>
                <div class="col-6">
                    <br>
                    You will no see something lik this:
                </div>
                <div class="col-6">
                    <br>
                    <img src="images/tutorial/contact2.png" class="d-block w-100 video-foto-mainpage" alt="...">
                </div>
                <div class="col-6">
                    <br>
                    <img src="images/tutorial/contact3.png" class="d-block w-100 video-foto-mainpage" alt="...">
                </div>

                <!-- cources show cases -->
                <div class="col-6">
                    <H3>Step 3</H3>
                    <br>
                    navigate to "overview"
                </div>
                <div class="col-6">
                    <br>
                    <img src="images/tutorial/overview1.png" class="d-block w-100 video-foto-mainpage" alt="...">
                </div>
                <div class="col-6">
                    <br>
                    You will no see something lik this:
                </div>
                <div class="col-6">
                    <br>
                    <img src="images/tutorial/overview2.png" class="d-block w-100 video-foto-mainpage" alt="...">
                </div>
                <div class="col-6">
                    <br>
                    <img src="images/tutorial/overview3.png" class="d-block w-100 video-foto-mainpage" alt="...">
                </div>

                <!-- creator pages -->
                <div class="col-6">
                    <H3>Step creator only</H3>
                    <br>
                    creator dashboard
                </div>
                <div class="col-6">
                    <br>
                    <img src="images/tutorial/creator1.png" class="d-block w-100 video-foto-mainpage" alt="...">
                </div>
                <div class="col-6">
                    <br>
                    Now you will see something like this IF you're a creator!:
                </div>
                <div class="col-6">
                    <br>
                    <img src="images/tutorial/creator2.png" class="d-block w-100 video-foto-mainpage" alt="...">
                </div>
                <div class="col-6">
                    <br>
                    <img src="images/tutorial/creator3.png" class="d-block w-100 video-foto-mainpage" alt="...">
                </div>
            </div>
        </Section>


        <!-- light grey bar (update/patches tab) -->
        <section class="section bg-secondary text-light p-5">
            <div class="container">
                <div class="d-md-flex justify-content-between align-items-center">
                    <h3 class="mb-3 mb-md-0"></h3>
                </div>
            </div>
        </section>



    <!--Footer-->    
    <!-- <?php require 'views/utils/footer.php'; ?> -->

      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
  
    </body>
    
</html>