
<html lang="en">
<head>


    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" href="/views/css/course_specific.css">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Other Head Items -->
    <link rel="icon" type="image/x-icon" href="/images/logo-header-home.webp"> <!-- IMG in browser tab -->
    <title><?php echo $course[0]->course; ?></title> <!-- Text in browser tab -->

</head>


<body>
<?php
require 'utils/navigation.php';
require 'views/utils/LoginCheck.php';
?>

<!-- If the User has not finished the Git Course yet, show this banner. -->
<?php if ($_SESSION["git_done"] == 0) {
    require 'utils/git_alert.php';
}
?>

<!-- Breakcrumb -->
<div id="breadcrumb">
    <div class="container m-0">
        <div class="col-md-6 col-12">
            <div class="row">
                <a href="courseoverzicht"><-- Terug naar course Overzicht</a>
            </div> <!-- Row -->
        </div> <!-- Col -->
    </div> <!-- Con -->
</div> <!--ID -->
<br>
<br>
<br>
<br>

<?php
echo 'Voorbeeld van een exercise' . '<br>';
$question_num = 1;
$question_title = 'Question 1';
$question = 'how much is 64 x 1?';
$answer = 64;
require 'utils/course/exercise.view.php';
?>


<!-- Small Promo -->
<div id="course_main">
    <div class="container-fluid py-3">
        <div class="row justify-content-center g-0">
            <div class="col-md-3 col-9">
                <img src="<?php echo $course[0]->img; ?>" class="img-fluid" alt="...">
                <div>
                <iframe width="560" height="315" src="<?php echo $course[0]->vid ?>
                "title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe>
                </div>
            </div> <!-- Col -->
        </div> <!-- Row -->
    </div> <!-- Con -->
</div> <!-- ID -->


<!-- /////////// courseName /////////// -->
<div class="container pb-3">
    <div class="col-md-3 col-9">
       <h1><?php echo $course[0]->course; ?></h1> 
    </div> <!-- Col -->
</div> <!-- Con -->


<!-- /////////// course Title /////////// -->
<div class="container">
    <div class="col-md-3 col-9">
    <H3><?php echo $course[0]->course_title;?></H3>
    </div> <!-- Col -->
</div> <!-- Con -->

<!-- /////////// course description /////////// -->
<div class="container">
    <div class="col-md-3 col-9">
    <H3><?php echo $course[0]->course_description;?></H3>
    </div> <!-- Col -->
</div> <!-- Con -->

<!-- /////////// course extra description /////////// -->
<div class="container">
    <div class="col-md-3 col-9">
    <H3><?php echo $course[0]->extra_description;?></H3>
    </div> <!-- Col -->
</div> <!-- Con -->



<!-- /////////// Author /////////// -->
<div class="container">
    <div class="col-md-3 col-9">
    <H3> Author for this course: <?php echo $course[0]->author;?></H3>
    </div> <!-- Col -->
</div> <!-- Con -->

<!-- /////////// keywords /////////// -->
<div class="container">
    <div class="col-md-3 col-9">
    <H3> Author for this course: <?php echo $course[0]->keyword;?></H3>
    </div> <!-- Col -->
</div> <!-- Con -->



<?php require 'utils/bottom_bar.php'; ?>
<?php require 'utils/footer.php'; ?>

</body>
</html>