<?php
@session_start();

/* Redirects to no_search_match.view if no hits are found upon filtering */
if (empty(@$courses or $coursesF)) {
    header('Location: geen_match');
}

/* Decides if Company or Consumer pages are loaded */
if (@$_SESSION["User_type_id"] == 3) {
    $course_type = 'tray';
} else {
    $course_type = 'ce';
}

/* Assists the search bar & filter */
if (empty($coursesF)) {
    $filter_switch = $courses;
} else {
    $filter_switch = $coursesF;
}

/* We can use this to create a function @ the bottom of the page to give a message if NO/or a certain amount of courses are found. */
/* To prevent a page that feels too empty */
$deathCounter = 0;
?>

<html lang="en">
<head>


    <!-- CSS -->
    <link rel="stylesheet" href="/views/css/course_overview.css">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Other Head Items -->
    <link rel="icon" type="image/x-icon" href="/images/logo-header-home.webp"> <!-- IMG in browser tab -->
    <title>Courses</title> <!-- Text in browser tab -->
</head>


<body>

<?php
// Navbar
require 'utils/navigation.php';
// LoginCheck
require 'views/utils/LoginCheck.php';
?>

<!-- If the User has not finished the Git Course yet, show this banner. -->
<?php if (@$_SESSION["git_done"] == 0) {
    require 'utils/git_alert.php';
}
?>



<!-- /////////// Homescreen ///////////-->
<div id="homescreen">
    <div class="container-fluid p-0">
        <picture>
            <source srcset="/images/study_main.jpg" media="(max-width: 767px)">
            <source srcset="/images/study_main.jpg">
            <img src="/images/study_main.jpg" alt="...">
        </picture>
    </div>
</div>


<!-- /////////// Filter & Search /////////// -->
<div id="searchbar">
    <div class="container py-3">
        <div class="row justify-content-center">
            <div class="col-md-6 col-12">

                <!-- Search Bar -->
                <?php
                $form_action = 'courseoverzicht-search';
                require 'utils/filters/search.php';
                ?>

                <!-- Button Setup -->
                <?php
                $form_action = 'coursesfilter';
                $form_column = 'Category_id';
                ?>

                <!--  Vanaf hier buttons positioning fixen              -->
                <div class="row">


                    <!-- GIT -->
                    <?php
                    $form_value = 1;
                    $form_name = 'GIT';
                    require 'utils/filters/category_filter.php';
                    ?>

                    <!-- Front-End -->
                    <?php
                    $form_value = 2;
                    $form_name = 'Front-End';
                    require 'utils/filters/category_filter.php';
                    ?>

                    <!-- Back-End -->
                    <?php
                    $form_value = 3;
                    $form_name = 'Back-End';
                    require 'utils/filters/category_filter.php';
                    ?>

                    <!-- Reset the course table -->
                    <?php
                    $form_action = 'courseoverzicht';
                    $form_name = 'Reset';
                    require 'utils/filters/category_filter.php';
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- /////////// course Cards & Promo's /////////// -->

<div id="git_jump"></div>
<?php
$category_check = 1;
require 'utils/course/course_cards_horizontal.php';
?>

<!-- Small Promo -->
<?php //require 'utils/course/small_promo.php' ?>

<!-- Big Promo -->
<?php //require 'utils/course/big_promo.php' ?>

<!-- Ijs Row -->
<?php
$category_check = 2;
require 'utils/course/course_cards_horizontal.php';
?>

<!-- Makkelijk mee te nemen Row -->
<?php
$category_check = 3;
require 'utils/course/course_cards_horizontal.php';
?>

<!-- Death Counter-->
<?php
if ($deathCounter >= 2) {
    echo 'Het lijkt erop dat je zoekopdracht helaas tot weinig courses heeft geleid. Wil je het ' . "<a href='courseoverzicht'>opnieuw proberen?</a>";
}
?>

<!-- Twitter -->
<?php require 'utils/socialmedia/twitter.php'; ?>


<!-- Facebook -->
<?php require 'utils/socialmedia/facebook.php'; ?>


<!-- Footer -->
<?php require 'utils/footer.php'; ?>

</body>
</html>