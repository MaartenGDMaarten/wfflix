<?php
@session_start();
?>

<html lang="en">
<head>


    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" href="/views/css/course_overview.css">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Other Head Items -->
    <link rel="icon" type="image/x-icon" href="/images/logo-header-home.webp"> <!-- IMG in browser tab -->
    <title>Helaas..</title> <!-- Text in browser tab -->

</head>


<body>
<?php
require 'utils/navigation.php';
require 'views/utils/LoginCheck.php';
?>
<!-- Homescreen-->
<div id="homescreen">
    <div class="container-fluid p-0">
        <picture>
            <source srcset="/images/no_search_result_mob.webp" media="(max-width: 767px)">
            <source srcset="/images/no_search_result.webp">
            <img src="/images/no_search_result.webp" alt="...">
        </picture>
        <!--        <button type="button" class="btn btn-outline-primary"><a href="about">Nieuw</a></button>-->
    </div>
</div>


<div class="container py-3">
    <div class="row justify-content-center">
        <div class="col-md-6 col-12">
<h5> <a href=""></a> </h5>

        </div>
    </div>
</div>


    <?php require 'utils/footer.php'; ?>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8"
            crossorigin="anonymous"></script>

</body>

</html>