<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" href="/views/css/course_overview.css">

    <!-- Other Head Items -->
    <link rel="icon" type="image/x-icon" href="/images/logo-header-home.webp"> <!-- IMG in browser tab -->
    <title>404 Not Found!</title>
</head>
<body>
<!-- Navigation -->
<?php require 'utils/navigation.php';
?>

<!-- End Navigation -->

<!-- 404 Image -->
<div id="homescreen">
    <div class="container-fluid col-8 p-0">
        <picture>
            <source srcset="/images/404_mob.webp" media="(max-width: 767px)">
            <source srcset="/images/404.webp">
            <img src="/images/404.webp" alt="...">
        </picture>
        Uuuhhhhh.. we konden de weg niet vinden. Onze excuses.
    </div>
</div>


</body>
</html>