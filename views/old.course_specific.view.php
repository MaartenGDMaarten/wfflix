<html lang="en">
<head>


    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" href="/views/css/course_specific.css">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Other Head Items -->
    <link rel="icon" type="image/x-icon" href="/images/logo-header-home.webp"> <!-- IMG in browser tab -->
    <title><?php echo $course[0]->course; ?></title> <!-- Text in browser tab -->

</head>


<body>
<?php
require 'utils/navigation.php';
require 'views/utils/LoginCheck.php';
?>

<?php //if(@$_SESSION["User_type_id"] == 3){
//    echo 'Hello Company, DIT WEGHALEN VOOR DE PRESENTATIE';
//}
//else {
//    echo 'Hello Consumer/Admin/Guest, DIT WEGHALEN VOOR DE PRESENTATIE';
//}
//?>

<!-- Breakcrumb -->
<div id="breadcrumb">
    <div class="container m-0">
        <div class="col-md-6 col-12">
            <div class="row">
                <a href="courseoverzicht"><-- Terug naar course Overzicht</a>
            </div> <!-- Row -->
        </div> <!-- Col -->
    </div> <!-- Con -->
</div> <!--ID -->


<!-- Search Bar Placeholder  -->
<div id="searchbar">
    <div class="container py-3">
        <div class="row justify-content-center">
            <div class="col-md-6 col-12">
                <div class="input-group">
                    <input type="search" class="form-control rounded" placeholder="Search (placeholder)"
                           aria-label="Search"
                           aria-describedby="search-addon"/>
                    <button type="button" class="btn btn-outline-primary">search</button>
                </div> <!-- Input -->
            </div> <!-- Col -->
        </div> <!-- Row -->
    </div> <!-- Con -->
</div> <!--ID -->


<!-- Small Promo -->
<div id="course_main">
    <div class="container-fluid py-3">
        <div class="row justify-content-center g-0">
            <div class="col-md-3 col-9">
                <img src="<?php echo $course[0]->img; ?>" class="img-fluid" alt="...">


                <!-- Badges -->
                <?php
                // Bestseller
                if ($course[0]->badge_bestseller == 0)
                    echo '<span class="badge bg-success">Bestseller</span>';

                // New
                if ($course[0]->badge_new == 1)
                    echo '<span class="badge bg-success">New</span>';


                // Unavailable
                if ($course[0]->badge_available == 1)
                    echo '<span class="badge bg-success">Available</span>';

                // Natural
                if ($course[0]->badge_natural == 1)
                    echo '<span class="badge bg-success">Natural</span>';
                ?>
            </div> <!-- Col -->
        </div> <!-- Row -->
    </div> <!-- Con -->
</div> <!-- ID -->


<!-- /////////// courseName /////////// CHANGE NAMMMEESSSSS-->
<div class="container pb-3">
    <div class="col-md-3 col-9">
        <?php echo $course[0]->course; ?>
    </div> <!-- Col -->
</div> <!-- Con -->


<!-- /////////// course Price /////////// CHANGE NAMMMEESSSSS-->
<div class="container">
    <div class="col-md-3 col-9">
        Prijs: €<?php echo $course[0]->unit_price; ?>
    </div> <!-- Col -->
</div> <!-- Con -->


<?php if(@$_SESSION["User_type_id"] == 3){ ?>
    <!-- /////////// course Price excl btw/////////// CHANGE NAMMMEESSSSS-->
<div class="container">
    <div class="col-md-3 col-9">
        Prijs (Excl. BTW): €<?php echo number_format(($course[0]->unit_price * (1 - 0.09)), 2); ?>
    </div> <!-- Col -->
</div> <!-- Con -->
<?php
} ?>

<!-- /////////// Volume /////////// CHANGE NAMMMEESSSSS-->
<div class="container pb-3">
    Inhoud: <?php echo $course[0]->unit_volume; ?><?php echo $course[0]->unit_volume_type; ?>
</div> <!-- Con -->

<!-- /////////// courseDescription /////////// CHANGE NAMMMEESSSSS-->
<div class="container pb-3">
    course informatie: <br><?php echo $course[0]->course_description; ?>
</div> <!-- Con -->

<!-- /////////// Allergens /////////// CHANGE NAMMMEESSSSS-->
<div class="container">
    Allergenen: <?php echo $course[0]->allergen; ?>
</div> <!-- Con -->


<!-- /////////// Ingredients /////////// CHANGE NAMMMEESSSSS-->
<div class="container pb-3">
    Ingredienten: <?php echo $course[0]->ingredient; ?>
</div> <!-- Con -->


<!-- /////////// Usage /////////// CHANGE NAMMMEESSSSS-->
<div class="container">
    Gebruik: <?php echo $course[0]->instruction; ?>
</div> <!-- Con -->


<!-- /////////// Storage /////////// CHANGE NAMMMEESSSSS-->
<div class="container pb-3">
    Bewaren: <?php echo $course[0]->storage; ?>
</div> <!-- Con -->


<!-- /////////// NutritionLabel /////////// CHANGE NAMMMEESSSSS-->

<div id="nutrition_label">
    <div class="container pb-3">
        <table class="table table-sm">
            <thead>
            <tr>
                <th>Voedingstabel</th>
                <th>per 100<?php echo $course[0]->unit_volume_type; ?></th>
            </tr>
            </thead>

            <tbody>
            <tr>
                <td>
                    Energie
                </td>
                <td>
                    <?php echo $course[0]->energy; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Totaal vet
                </td>
                <td>
                    <?php echo $course[0]->fat; ?>
                </td>
            </tr>
            <tr class="sub-item">
                <td>
                    waarvan Verzadigd vet
                </td>
                <td>
                    <?php echo $course[0]->s_fat; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Totaal koolhydraten
                </td>
                <td>
                    <?php echo $course[0]->carb; ?>
                </td>
            </tr>
            <tr class="sub-item">
                <td>
                    waarvan Suiker
                </td>
                <td>
                    <?php echo $course[0]->sugar; ?>
                </td>
            </tr>

            <tr>
                <td>
                    Eiwit
                </td>
                <td>
                    <?php echo $course[0]->protein; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Zout
                </td>
                <td>
                    <?php echo $course[0]->salt; ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<!-- /////////// Text 2 /////////// CHANGE NAMMMEESSSSS-->
<div class="container pb-3">
    Extra Information: <br><?php echo $course[0]->extra_description; ?>
</div>





<?php require 'utils/bottom_bar.php'; ?>
<?php require 'utils/footer.php'; ?>




</body>

</html>