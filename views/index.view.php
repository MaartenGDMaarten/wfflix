<?php if (@$_SESSION["User_type_id"] <> 99) { // Check User_type_id
header("Location: courseoverzicht"); // Redirects to root.
exit(); // Kill script
} ?>

<!-- REDIRECTED NAAR COURSEOVERZICHT VANWEGE TIJDSNOOD -->
<!-- REDIRECTED NAAR COURSEOVERZICHT VANWEGE TIJDSNOOD -->
<!-- REDIRECTED NAAR COURSEOVERZICHT VANWEGE TIJDSNOOD -->

<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" href="/views/css/home.css">

    <!-- Other Head Items -->
    <link rel="icon" type="image/x-icon" href="/images/logo-header-home.webp"> <!-- IMG in browser tab -->
    <title>Homepage</title>
</head>
<body>
<!-- Navigation -->
<?php
require 'utils/navigation.php';
require 'views/utils/LoginCheck.php';
?>
<!-- End Navigation -->

<!-- REDIRECTED NAAR COURSES VANWEGE TIJDSNOOD -->


<section>
    <!---About us intro  ---->
    <div class="Home">

        <h1></h1>
    </div>
    <!---About us intro End ---->
</section>

<section>

    <!---Our Story ---->
    <div class="hero-image">
        <div class="hero-text">
            <h1 style="font-size:50px">Welkom</h1>
            <p>Bekijk nu onze nieuwe assortiment!</p>
            <button id="myButton" class="float-left submit-button">Bekijken</button>
            <script type="text/javascript">
                document.getElementById("myButton").onclick = function () {
                    location.href = "http://localhost:9000/productoverzicht";
                };
            </script>
        </div>
    </div>


</section>
<section>
    <!---Our Employees ---->

    <div class="Welkom container-fluid"
    <div class="container mt-5">
        <h3>Best Sellers</h3>
        <div class="row">
            <div class="col-sm-2">
                <div class="card" style="width: 200px">
                    <img class="card-img-top" src="/images/huisstijl/Flevosap-appel-aardbei.jpg" alt="Card image"
                         style="width: 100%">
                    <div class="card-body">
                        <h5 class="card-title">Appel aardbei</h5>
                        <p class="card-text">Voedingswaarde per 100 gram
                            Energie: 194 kJ (46 kcal)
                            Vetten: 0,0
                        <hr>
                        <button type="button" onclick="alert('Hello world!')">Koop nu!</button>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="card" style="width: 200px">
                    <img class="card-img-top" src="/images/huisstijl/Flevosap-appel-peer.jpg" alt="Card image"
                         style="width: 100%">
                    <div class="card-body">
                        <h5 class="card-title">Appel peer</h5>
                        <p class="card-text">Voedingswaarde per 100 gram
                            Energie: 194 kJ (46 kcal)
                            Vetten: 0,0
                        <hr>
                        <button type="button" onclick="alert('Hello world!')">Koop nu!</button>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="card" style="width: 200px">
                    <img class="card-img-top" src="/images/huisstijl/Flevosap-appel-kers.jpg" alt="Card image"
                         style="width: 100%">
                    <div class="card-body">
                        <h5 class="card-title">Appel kers</h5>
                        <p class="card-text">Voedingswaarde per 100 gram
                            Energie: 194 kJ (46 kcal)
                            Vetten: 0,0
                        <hr>
                        <button type="button" onclick="alert('Hello world!')">Koop nu!</button>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="card" style="width: 200px">
                    <img class="card-img-top" src="/images/huisstijl/Flevosap-sinaasappel.jpg" alt="Card image"
                         style="width: 100%">
                    <div class="card-body">
                        <h5 class="card-title">Sinaasappel</h5>
                        <p class="card-text">Voedingswaarde per 100 gram
                            Energie: 194 kJ (46 kcal)
                            Vetten: 0,0
                        <hr>
                        <button type="button" onclick="alert('Hello world!')">Koop nu!</button>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="card" style="width: 200px">
                    <img class="card-img-top" src="/images/huisstijl/Flevosap-Appel-Ananas-Perzik.png" alt="Card image"
                         style="width: 100%">
                    <div class="card-body">
                        <h5 class="card-title">Appel ananas perzik</h5>
                        <p class="card-text">Voedingswaarde per 100 gram
                            Energie: 194 kJ (46 kcal)
                            Vetten: 0,0
                        <hr>
                        <button type="button" onclick="alert('Hello world!')">Koop nu!</button>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="card" style="width: 200px">
                    <img class="card-img-top" src="/images/huisstijl/Flevosap-200-ml-Appel-Zwartebes.jpg"
                         alt="Card image" style="width: 100%">
                    <div class="card-body">
                        <h5 class="card-title">Appel zwartebes</h5>
                        <p class="card-text">Voedingswaarde per 100 gram
                            Energie: 194 kJ (46 kcal)
                            Vetten: 0,0
                        <hr>
                        <button type="button" onclick="alert('Hello world!')">Koop nu!</button>
                        </p>

                    </div>
                </div>
                <br>
            </div>
        </div>
        <!---Our Empoloyees End ---->
</section>

<div class="Welkom container-fluid"
<div class="container mt-5 ">
    <div class="row">
        <div class="col-sm-6 mx-5 ">
            <h1></h1>
            <h3>Liefhebbers van Flevosap: welkom!</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis commodo tortor ut nisi tempor,
                non dignissim massa consectetur. Sed vitae tellus a lectus lobortis pellentesque.
                In sed pharetra eros. Etiam sodales luctus pretium. Morbi fringilla posuere nulla ut luctus.
                Vestibulum feugiat odio non nisl iaculis, ac blandit nisi placerat. Sed eu porta erat.
                Maecenas pretium euismod sapien vitae auctor. Aenean a nisi at erat efficitur tincidunt a ac
                nibh.</p>
            <br>
        </div>
    </div>
</div>
</div>

<!---Our Story End ---->
<!---Our Products---->
<div class="Welkom1 container-fluid"
<div class="container mt-5">
    <div class="row">
        <div class="col-sm-4">
            <img class="card-img-top" src="/images/huisstijl/lunchboxC.png" alt="Card image"
                 style="width: 100%">

        </div>
        <div class="col-sm-8">
            <h3>Over ons</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis commodo tortor ut nisi
                tempor,
                non dignissim massa consectetur. Sed vitae tellus a lectus lobortis pellentesque.
                In sed pharetra eros. Etiam sodales luctus pretium. Morbi fringilla posuere nulla ut
                luctus.
                Vestibulum feugiat odio non nisl iaculis, ac blandit nisi placerat. Sed eu porta erat.
                Maecenas pretium euismod sapien vitae auctor. Aenean a nisi at erat efficitur tincidunt
                a ac
                nibh.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis commodo tortor ut
                nisi tempor,
                non dignissim massa consectetur. Sed vitae tellus a lectus lobortis pellentesque.
                In sed pharetra eros. Etiam sodales luctus pretium. Morbi fringilla posuere nulla ut
                luctus.
                Vestibulum feugiat odio non nisl iaculis, ac blandit nisi placerat. Sed eu porta erat.
                Maecenas pretium euismod sapien vitae auctor. Aenean a nisi at erat efficitur tincidunt
                a ac
                nibh.</p>
                <button id="myButton2" class="float-left submit-button">Meer weten!</button>
                <script type="text/javascript">
                    document.getElementById("myButton2").onclick = function () {
                        location.href = "http://localhost:9000/over-ons";
                    };
                </script>
        </div>
    </div>
    <!---Our Products End ---->
    <img src="images/huisstijl/Home-350px.jpg" height="100">

    <section>
        <img src="images/huisstijl/Schermafbeelding.jpg">

        <!-- Footer -->
        <?php require 'utils/footer.php' ?>
        <!-- End Footer -->

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
                integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
                crossorigin="anonymous"></script>
</body>
</html>