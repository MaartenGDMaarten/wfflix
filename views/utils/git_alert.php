<link rel="stylesheet" href="/views/css/warning_banner.css">

<div class="alert warning sticky-top">
    <span class="closebtn">&times;</span>
    <form id="form__submit" action="git-done" method="post">
        <input type="hidden" name="gitDone" id="gitDone" value="1">
        <input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['userid']; ?>">
        <strong>Let op!</strong> We raden je aan om eerst de <a href="#git_jump">GIT Course</a> te voltooien. Geen interesse?
        <a href="#" onclick="submit_form()">Klik hier</a> om dit bericht te verwijderen.
    </form>
</div>

<script>
    <?php require 'scripts/submit_form.js';
    require 'scripts/close_warning.js'; ?>
</script>




