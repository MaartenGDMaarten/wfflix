<?php
@session_start();

// Link database
$controller = new Connection;
App::bind('config', require 'config.php');
$conn = Connection::make(App::get('config'));

// Set variables
if(isset($_POST['contact_form'])) {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $email = $_POST['email'];
        $subject = $_POST['subject'];
        $category = $_POST['category'];
        $comment = $_POST['comment'];


        // Insert data
        $sql = "INSERT INTO contact_form (first_name, last_name, email, subject, category, comment) 
        VALUES ('$first_name', '$last_name', '$email', '$subject', '$category', '$comment')";

        $stmt = $conn->prepare($sql);
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>test for,</title>
</head>
<body>
<section class="col-5 bg-secondary text-white p-3 m-5">
    <!-- contact txt -->
    <h1>Zoek contact</h1>
    <form action="contact" method="POST">    <!-- name for info -->
        <div class="row mt-2">
            <div class="col">
                <input type="text" class="form-control" placeholder="Voornaam" aria-label="First name" name="first_name" id="first_name" required>
            </div>
            <div class="col">
                <input type="text" class="form-control" placeholder="Achternaam" aria-label="Last name" name="last_name" id="last_name" required>
            </div>
        </div>
        <!-- fill in email -->
        <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">Email</label>
            <input type="email" class="form-control" placeholder="wfflix@voorbeeld.nl" name="email" id="email" required>
        </div>
        <!-- add title/label -->
        <div class="mb-3">
            <label for="formGroupExampleInput" class="form-label">Onderwerp</label>
            <input type="text" class="form-control" placeholder="Ik mis een course." name="subject" id="subject" required>
        </div>
        <!-- see faq to repport on -->
        <select class="form-select" aria-label="Default select example" name="category" id="category" required>
            <option selected>Categorie</option>
            <option value="Product">Course</option>
            <option value="Verzending">Verzending</option>
            <option value="Betaling">Betaling</option>
            <option value="Klacht">Klacht</option>
            <option value="Overig">Overig</option>
        </select>
        <!-- comment section -->
        <div class="form-floating">
            <textarea class="form-control mt-2" placeholder="Leave a comment here" name="comment" id="comment" required></textarea>
            <label for="comment">Comment:</label>
        </div>
        <!-- submit buton -->
        <button type="submit" value="Submit" class="btn btn-success mt-2 " name="contact_form">Verstuur</button>
    </form>

    <?php

    if(isset($_POST['contact_form'])) {
        try {
            $conn->exec($sql);
            echo "Uw bericht is ontvangen";
        } catch (PDOException $e) {
            echo "Er is iets misgegaan";
        }
    }

    ?>
</section>

</body>
</html>