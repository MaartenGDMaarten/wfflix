<?php

// Check if the user is already logged in, if not then redirect him to the login page
if($_SESSION["loggedin"] == null){
    header("location: login");
    exit;
}
?>
