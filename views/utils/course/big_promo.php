<?php
 echo '

<!-- Als we meer hebben om te promoten kunnen we dit als database inladen zoals de course cards, dus aan de hand van een database. -->
<div class="big_promo">
    <div class="container-fluid py-3">
        <div class="row justify-content-center g-0">
            <div class="col-lg-3 col-md-3 col-sm-5 col-6 p-3">
                <img src="/images/dbimg/67-smoothie.webp" class="img-fluid" alt="...">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-5 col-6 p-3">
                <b class="card-title">Smoothie Basis Sap!</b>
                <p class="card-text">Begin je dag goed met de Smoothie Basis Sap. <br><br>Schenk 250ml Smoothie
                    Basis
                    Sap in de blender en gooi er 120g fruit bij. Even mixen en klaar!</p>
            </div>
        </div>
    </div>
</div>
'

 ?>