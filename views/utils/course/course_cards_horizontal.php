<?php

if ($category_check == 1) {
    $title = 'Git';
    $divclass = 'four_in_row';
    $head = 'normalhead';
} elseif ($category_check == 2) {
    $title = 'Front-End';
    $divclass = 'red_in_row';
    $head = 'redhead';
} elseif ($category_check == 3) {
    $title = 'Back-End';
    $divclass = 'red_in_row';
    $head = 'redhead';
}

$counter = 0;
$switchCounter = 0;

foreach ($filter_switch as $course) :
    if ($course->Category_id == $category_check) {
        $counter++;
    }
endforeach;
?>

<div id="<?php echo "$head"; ?>">
    <?php if ($counter >= 1) { ?>
        <h2 class="text-center"><?php echo "$title"; ?></h2>
    <?php } else {
        $deathCounter++;
    } ?>
</div>

<div class="<?php echo "$divclass"; ?>">
    <div class="container">
        <div class="row justify-content-center p-1">


            <?php
            //                Dit is voor de categorie rows
            foreach ($filter_switch

                     as $course) :
                if ($course->Category_id == $category_check) {
                    if ($switchCounter % 2 == 0) { ?>

                        <div class="col-12 p-2">

                            <div class="row no-gutters">
                                <div class="col-md-4">
                                    <img src="<?php echo $course->img; ?>" class="card-img card h-100" alt="...">
                                </div>
                                <div class="col-md-8">
                                    <div class="card h-100 text-center">
                                        <div class="card-body">
                                            <h5 class="card-title"><?php echo $course->course_title; ?></h5>
                                            <p class="card-text"><?php echo $course->course_description; ?></p>
                                            <p class="card-text"><?php echo $course->extra_description; ?></small></p>
                                            <form action="coursepagina" method="post">
                                                <button type="submit" name="id" value="<?= $course->Course_id; ?>"
                                                        class="btn btn-primary">
                                                    Shop
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <?php
                        $switchCounter++;
                    } else { ?>


                        <div class="col-12 p-2">

                            <div class="row no-gutters">

                                <div class="col-md-8">
                                    <div class="card h-100 text-center">
                                        <div class="card-body">
                                            <h5 class="card-title"><?php echo $course->course_title; ?></h5>
                                            <p class="card-text"><?php echo $course->course_description; ?></p>
                                            <p class="card-text"><?php echo $course->extra_description; ?></small></p>
                                            <form action="coursepagina" method="post">
                                                <button type="submit" name="id" value="<?= $course->Course_id; ?>"
                                                        class="btn btn-primary">
                                                    Shop
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <img src="<?php echo $course->img; ?>" class="card-img card h-100" alt="...">
                                </div>

                            </div>
                        </div>


                        <?php $switchCounter++;
                    }


                }
            endforeach;

            ?>


        </div>
    </div>
</div>