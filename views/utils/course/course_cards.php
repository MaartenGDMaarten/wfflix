<?php

if ($category_check == 1) {
    $title = 'Git';
    $divclass = 'four_in_row';
    $head = 'normalhead';
} elseif ($category_check == 2) {
    $title = 'Front-End';
    $divclass = 'red_in_row';
    $head = 'redhead';
} elseif ($category_check == 3) {
    $title = 'Back-End';
    $divclass = 'red_in_row';
    $head = 'redhead';
}

$counter = 0;

foreach ($filter_switch as $course) :
        if ($course->Category_id == $category_check) {
            $counter++;
    }
endforeach;
?>

<div id="<?php echo "$head"; ?>">
    <?php if ($counter >= 1) { ?>
        <h2 class="text-center"><?php echo "$title"; ?></h2>
    <?php } else {
        $deathCounter++;
    } ?>
</div>

<div class="<?php echo "$divclass"; ?>">
    <div class="container">
        <div class="row justify-content-center p-1">


            <?php
            //                Dit is voor de categorie rows
            foreach ($filter_switch as $course) :
                    if ($course->Category_id == $category_check) { ?>

                        <div class="col-md-3 col-sm-5 col-6 p-2">
                            <div class="card h-100 text-center">

                                <img src="<?php echo $course->img; ?>" alt="...">
                                <div class="card-body">
                                    <h2 class="card-title"><?php echo $course->course; ?></h2>
                                    <p class="card-text">Per stuk: €<?php echo $course->unit_price; ?></p>
                                    <p class="card-text">Inhoud:
                                        <?php echo $course->unit_volume;
                                        echo $course->unit_volume_type; ?>
                                    </p>
                                    <form action="coursepagina" method="post">
                                        <button type="submit" name="id" value="<?= $course->Course_id; ?>"
                                                class="btn btn-primary">
                                            Shop
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>


                        <?php

                }
            endforeach;

            ?>


        </div>
    </div>
</div>