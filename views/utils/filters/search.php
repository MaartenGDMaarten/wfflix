<form action="<?php echo $form_action ?>" method="post">
    <div class="input-group">
        <input type="text" class="form-control rounded"
               placeholder="Zoek een course.."
               aria-label="Search" name="RowValue" value="">
        <input type="hidden" name="ColumnName" value="keyword">
        <button type="submit" class="btn btn-outline-primary" value="submit">Zoek</button>
    </div>
</form>