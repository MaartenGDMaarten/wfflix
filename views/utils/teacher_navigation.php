<!-- Navigation -->

<style>
    @import url("https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css");

    a {
        color:darkred;
    }

    a:hover {
        color:indianred;
    }

    nav {
        border-bottom-style: ridge;
        border-top-width: 1px;
        background-color: #87D1EE;
    }
</style>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

<nav class="navbar navbar-expand-md sticky-top">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">

            <img src="/images/logo-header-home.webp" alt="" width="32 " height="32" class="d-inline-block align-text-top">
            TEACHER DASHBOARD</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="teacher_courses">Courses</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="teacher_help">Help</a>
                </li>


                <li class="nav-item">
                    <a href="login"><i class="bi bi-person-circle fs-1 ms-3"></i></a>
                </li>
                <?php if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) { ?>

                    <li class="nav-item">
                        <a href="logout" class="btn btn-danger ml-3">Log uit</a>
                    </li>
                <?php } ?>
            </ul>

        </div>
    </div>
</nav>

<!--Trigger Dropdown -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8"
        crossorigin="anonymous"></script>
<!-- End Navigation -->