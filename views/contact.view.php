<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" href="/views/css/contact.css">

    <!-- Other Head Items -->
    <link rel="icon" type="image/x-icon" href="/images/logo-header-home.webp"> <!-- IMG in browser tab -->
    <title>Contact</title>
</head>
<body>
<!-- Navigation -->
<?php require 'utils/navigation.php';
// LoginCheck
require 'views/utils/LoginCheck.php';

?>
<!-- End Navigation -->

<section>
    <!---Contact intro  ---->
    <div class="contact">

        <h1>Contact </h1>

    </div>
    <!---Contact intro End ---->
</section>

<!-- Frequently Asked Questions accordion -->
<section id="questions" class="p-5">
    <div class="container">
        <div class="accordion" id="accordion">

            <div class="accordion-item">
                <h2 class="accordion-header" id="heading1">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
                        Lorem
                    </button>
                </h2>
                <div id="collapse1" class="accordion-collapse collapse" aria-labelledby="heading1" data-bs-parent="#accordion">
                    <div class="accordion-body">
                        <strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique sem nunc, at fermentum risus.</strong>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="heading2">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                        Ipsum
                    </button>
                </h2>
                <div id="collapse2" class="accordion-collapse collapse show" aria-labelledby="heading2" data-bs-parent="#accordion">
                    <div class="accordion-body">
                        <strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique sem nunc, at fermentum risus.</strong>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="heading3">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                        Dolor
                    </button>
                </h2>
                <div id="collapse3" class="accordion-collapse collapse" aria-labelledby="heading3" data-bs-parent="#accordion">
                    <div class="accordion-body">
                        <strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique sem nunc, at fermentum risus.</strong>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="heading4">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                        Sit
                    </button>
                </h2>
                <div id="collapse4" class="accordion-collapse collapse" aria-labelledby="heading4" data-bs-parent="#accordion">
                    <div class="accordion-body">
                        <strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique sem nunc, at fermentum risus.</strong>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<?php require 'utils/contact_form.php'; ?>

<section>
    <!---Our location ---->

    <div class="container mt-5">
        <div class="row">
            <div class="col-sm-8">
                <h3>Contact Us</h3>
                <p>Lorem Ipsum Press Lorem Ipsum PressLorem Ipsum PressLorem Ipsum PressLorem Ipsum Press
                </p>
            </div>
        </div>
        <div>
            <img class="card-img-top" src="/images/bionetwork.png" alt="Card image"
                 style="width: 50%">
        </div>
        <div>
            <p>
                Address:
                <br>
                Keas 69 Str.
                <br>
                15234, Bladiebla
                <br>
                De Hema,
                <br>
                Lekkah
                <br>
                <br>
                +30-1111111111 (landline)
                <br>
                +30-1111111111 (mobile phone)
                <br>
                +30-1111111111 (fax)
            </p>
        </div>
    </div>

</section>
<!---Our location End ---->

<!-- Footer -->
<?php require 'utils/footer.php' ?>
<!-- End Footer -->

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>

</body>
</html>