<?php

//Check if the user is an admin || If admin = give access || If anything else -> redirect to Root)
require 'views/utils/admin_security.php';

$UserIdCourse = $_SESSION['userid'];
?>

<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" type="image/x-icon" href="/images/logo-header-home.webp"> <!-- IMG in browser tab -->
    <title>Course Toevoegen</title>
</head>
<body>
<!-- Navigation -->
<?php require 'views/utils/admin_navigation.php'; ?>
<!-- End navigation -->


<h2>---------------------------------------- Voeg een image toe aan je folder -----------------------------------------</h2>
<form action="images/dbimg/uploadcourse.php" method="post" enctype="multipart/form-data">
    Select image to upload:
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Upload Image" name="submit">
</form>

<?php
if (!empty(@$_SESSION['UploadMessage'])){
    echo 'De afbeelding is correct geupload.' . '<br>' . 'Gebruik <b> /images/dbimg/' . @$_SESSION['UploadMessage'] . ' </b> om het te gebruiken bij een course.' ;
    $autofillimg = '/images/dbimg/' . @$_SESSION['UploadMessage'];
    @$_SESSION['UploadMessage'] = null;
} ?>

<h2>---------------------------------------- Voeg Course Toe -----------------------------------------</h2>
<!--<h3>Alle velden correct invullen anders crash, moet het nog beveiligen (werkt wel bij STephan)</h3>-->
<form action="add-course" method="post">
    <div class="mb-3">
        <label for="courseName" class="form-label">course Naam</label>
        <input type="text" class="form-control w-25" name="courseName" id="courseName" value="">
    </div>

    <div class="mb-3">
        <label for="courseTitel" class="form-label">course Titel</label>
        <input type="text" class="form-control w-25" name="courseTitel" id="courseTitel" value="">
    </div>

    <div class="mb-3">
        <label for="courseDescription" class="form-label">Course Beschrijving</label>
        <input type="text" class="form-control w-25" name="courseDescription" id="courseDescription" value="">
    </div>

    <select class="form-select w-25" aria-label="Default select example" id="Category_id" name="Category_id">
        <option selected>Categorie</option>
        <option value="1">1 = GIT</option>
        <option value="2">2 = Front-End</option>
        <option value="3">3 = Back-End</option>
    </select>

    <div class="mb-3">
        <label for="courseAuthor" class="form-label">courseAuthor</label>
        <input type="text" class="form-control w-25" name="courseAuthor" id="courseAuthor" value="">
    </div>

    <div class="mb-3">
        <label for="courseImg" class="form-label">courseImg</label>
        <input type="text" class="form-control w-25" name="courseImg" id="courseImg" value="<?= @$autofillimg ?>">
    </div>

    <div class="mb-3">
        <label for="courseVid" class="form-label">Video</label>
        <input type="text" class="form-control w-25" name="courseVid" id="courseVid" value="">
    </div>

    <div class="mb-3">
        <label for="courseVid_two" class="form-label">Video Twee</label>
        <input type="text" class="form-control w-25" name="courseVid_two" id="courseVid_two" value="">
    </div>

    <div class="mb-3">
        <label for="courseExercise" class="form-label">Oefening</label>
        <input type="text" class="form-control w-25" name="courseExercise" id="courseExercise" value="">
    </div>

    <div class="mb-3">
        <label for="courseExercise_two" class="form-label">Oefening Twee</label>
        <input type="text" class="form-control w-25" name="courseExercise_two" id="courseExercise_two" value="">
    </div>

    <div class="mb-3">
        <label for="courseAssociated" class="form-label">Gerelateerde Courses</label>
        <input type="text" class="form-control w-25" name="courseAssociated" id="courseAssociated" value="">
    </div>

    <div class="mb-3">
        <label for="courseAssociated_2" class="form-label">Gerelateerde Courses 2</label>
        <input type="text" class="form-control w-25" name="courseAssociated_2" id="courseAssociated_2" value="">
    </div>

    <div class="mb-3">
        <label for="courseExercise_two" class="form-label">Gerelateerde Courses 3</label>
        <input type="text" class="form-control w-25" name="courseExercise_two" id="courseExercise_two" value="">
    </div>

    <div class="mb-3">
        <label for="courseTime" class="form-label">Tijdsduur</label>
        <input type="time" class="form-control w-25" name="courseTime" id="courseTime" value="">
    </div>


    <div class="mb-3">
        <input type="hidden" class="form-control w-25" name="useridcourse" id="useridcourse" value="<?php echo $UserIdCourse; ?>">
    </div>
    <div class="mb-3">
        <input type="hidden" class="form-control w-25" name="courseid" id="courseid" value="">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>


</form>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>