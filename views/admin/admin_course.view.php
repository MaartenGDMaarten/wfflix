<?php

//Check if the user is an admin || If admin = give access || If anything else -> redirect to Root)
require 'views/utils/admin_security.php';

?>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" type="image/x-icon" href="/images/logo-header-home.webp"> <!-- IMG in browser tab -->
    <title>Courses</title>
</head>
<body>
<?php require 'views/utils/admin_navigation.php'; ?>
<h1>courses</h1>


<div class="container-fluid pb-5">
    <a href="admin-add-course">Voeg course toe</a>
</div>

<?php


// Base state
if (empty($coursesF)) {
    $filter_switch = @$courses;
} else {
    $filter_switch = $coursesF;
}
?>

<!-- Button Setup -->
<?php
$form_action = 'courses';
$form_column = 'Category_id';
?>

<!--  Vanaf hier buttons positioning fixen              -->
<div class="row">


    <!-- GIT -->
    <?php
    $form_value = 1;
    $form_name = 'GIT';
    require 'views/utils/filters/category_filter.php';
    ?>

    <!-- Front-End -->
    <?php
    $form_value = 2;
    $form_name = 'Front-End';
    require 'views/utils/filters/category_filter.php';
    ?>

    <!-- Back-End -->
    <?php
    $form_value = 3;
    $form_name = 'Back-End';
    require 'views/utils/filters/category_filter.php';
    ?>

    <!-- Reset the course table -->
    <?php
    $form_action = 'coursesreset';
    $form_name = 'Reset';
    require 'views/utils/filters/category_filter.php';
    ?>
</div>




    <!-- Search Bar -->
    <div class="p-1">
        <form action="coursessearch" method="post">
            <input type="text" name="RowValue" placeholder="Keyword" value="">
            <input type="hidden" name="ColumnName" value="keyword">
            <input type="submit" value="Filter"><br>
        </form>
    </div>



<!-- Reset the course table -->

<?php if (empty($courses)) { ?>
    <?php if (empty($coursesF)) { ?>
        <div class="form-inline justify-content-center">
            <div class="p-1">
                <form action="coursesreset" method="post">
                    <button type="submit" name="RowValue"
                            class="btn btn-danger">
                        <?php echo 'Geen Match, graag resetten en opnieuw proberen'; ?>
                    </button>
                    <input type="hidden" name="ColumnName">
                </form>
            </div>
        </div>
    <?php } ?>
<?php } ?>

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-11">
            <h2>courseOverzicht</h2>

            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Course</th>
                        <th scope="col">Category</th>
                        <th scope="col">Course Titel</th>
                        <th scope="col">Course omschrijving</th>
                        <th scope="col">Auteur</th>
                        <th scope="col">Img</th>
                        <th scope="col">Video</th>
                        <th scope="col">Oefening(en)</th>
                        <th scope="col">Gerelateerde Courses</th>
                        <th scope="col">Uren</th>
                        <th scope="col">Update</th>
                        <th scope="col">Delete</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    if (!empty($filter_switch)){
                    foreach ($filter_switch

                    as $course) : ?>


                    <tr>
                        <td><?= $course->Course_id
                            ; ?></td>
                        <td><?= $course->course; ?></td>
                        <td><?= $course->Category_id; ?></td>
                        <td><?= $course->course_title; ?></td>
                        <td><?= $course->course_description; ?></td>
                        <td><?= $course->author; ?></td>
                        <td>
                            <?php if (!empty ($course->img)) {
                                echo '| <a target="_target" href=' . $course->img . '>1</a> |';
                            } ?>
                            <?php if (!empty ($course->img_2)) {
                                echo ' <a target="_target" href=' . $course->img_2 . '>2</a> |';
                            } ?>
                            <?php if (!empty ($course->img_3)) {
                                echo ' <a target="_target" href=' . $course->img_3 . '>3</a> |';
                            } ?>
                            <?php if (!empty ($course->img_4)) {
                                echo ' <a target="_target" href=' . $course->img_4 . '>4</a> |';
                            } ?>
                            <?php if (!empty ($course->img_5)) {
                                echo ' <a target="_target" href=' . $course->img_5 . '>5</a> |';
                            } ?>
                        </td>
                        <td>
                            <?php if (!empty ($course->vid)) {
                                echo '| <a target="_target" href=' . $course->vid . '>1</a> |';
                            } ?>
                            <?php if (!empty ($course->vid_2)) {
                                echo ' <a target="_target" href=' . $course->vid_2 . '>2</a> |';
                            } ?>
                        </td>
                        <td>
                            <?php if (!empty ($course->exercise)) {
                                echo '| <a target="_target" href=' . $course->exercise . '>1</a> |';
                            } ?>
                            <?php if (!empty ($course->exercise_2)) {
                                echo ' <a target="_target" href=' . $course->exercise_2 . '>2</a> |';
                            } ?>
                        </td>
                        <td>
                            <?php if (!empty ($course->associated_course)) {
                                echo '| <a target="_target" href=' . $course->associated_course . '>1</a> |';
                            } ?>
                            <?php if (!empty ($course->associated_course_2)) {
                                echo ' <a target="_target" href=' . $course->associated_course_2 . '>2</a> |';
                            } ?>
                            <?php if (!empty ($course->associated_course_3)) {
                                echo ' <a target="_target" href=' . $course->associated_course_3 . '>3</a> |';
                            } ?>
                        </td>


                        <td><?= $course->hours; ?></td>


                        <td>
                            <form action="upd-course" method="post">
                                <button type="submit" name="id" value="<?= $course->Course_id
                                ; ?>"
                                        class="btn btn-primary">
                                    UPDATE
                                </button>
                            </form>
                        </td>
                        <td>
                            <form action="del-course" method="post">
                                <button type="submit" name="id" value="<?= $course->Course_id
                                ; ?>"
                                        class="btn btn-danger">
                                    DELETE
                                </button>
                            </form>
                        </td>
                    </tr>
                    </tbody>
                    <?php

                    endforeach;
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>

</body>
</html>
