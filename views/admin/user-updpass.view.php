<?php

//Check if the user is an admin || If admin = give access || If anything else -> redirect to Root)
require 'views/utils/admin_security.php';
?>

<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" type="image/x-icon" href="/images/logo-header-home.webp"> <!-- IMG in browser tab -->
    <title>Password Update</title>

</head>
<body>
<!-- Navigation -->
<?php require 'views/utils/admin_navigation.php'; ?>
<!-- End navigation -->


<form action="store-userPass" method="post">
    <div class="four_in_row">
        <div class="container py-3">
            <h2 class="text-center">Update Password van <?= $user[0]->username; ?> </h2>


            <b>Account Algemeen</b>
            <div class="row p-1">

                <!-- Password -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                    <div class="mb-3">
                        <label for="password" class="form-label">Wachtwoord</label>
                        <input type="password" class="form-control" name="password" id="password" pattern=".{8,}"
                               value="" title="Must contain at least 8 or more characters" required>
                    </div>
                </div>

            </div>


            <div class="mb-3">
                <input type="hidden" class="form-control" name="userid" id="userid"
                       value="<?= $user[0]->User_id; ?>">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>


        </div>
    </div>
</form>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</body>
</html>