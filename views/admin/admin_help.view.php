<?php

//Check if the user is an admin || If admin = give access || If anything else -> redirect to Root)
require 'views/utils/admin_security.php';

?>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="icon" type="image/x-icon" href="/images/logo-header-home.webp"> <!-- IMG in browser tab -->
    <title>Help</title>

</head>
<body>
<?php require 'views/utils/admin_navigation.php'; ?>
<h1>Help</h1>

<!-- Frequently Asked Questions accordion -->
<section id="questions" class="p-5">
    <div class="container">
        <div class="accordion" id="accordion">

            <div class="accordion-item">
                <h2 class="accordion-header" id="heading1">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
                        Lorem
                    </button>
                </h2>
                <div id="collapse1" class="accordion-collapse collapse" aria-labelledby="heading1" data-bs-parent="#accordion">
                    <div class="accordion-body">
                        <strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique sem nunc, at fermentum risus.</strong>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="heading2">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                        Ipsum
                    </button>
                </h2>
                <div id="collapse2" class="accordion-collapse collapse show" aria-labelledby="heading2" data-bs-parent="#accordion">
                    <div class="accordion-body">
                        <strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique sem nunc, at fermentum risus.</strong>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="heading3">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                        Dolor
                    </button>
                </h2>
                <div id="collapse3" class="accordion-collapse collapse" aria-labelledby="heading3" data-bs-parent="#accordion">
                    <div class="accordion-body">
                        <strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique sem nunc, at fermentum risus.</strong>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="heading4">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                        Sit
                    </button>
                </h2>
                <div id="collapse4" class="accordion-collapse collapse" aria-labelledby="heading4" data-bs-parent="#accordion">
                    <div class="accordion-body">
                        <strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique sem nunc, at fermentum risus.</strong>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


<a href="/docs/Einddocumentatie.docx" download>Einddocumentatie 1</a>


</body>
</html>
