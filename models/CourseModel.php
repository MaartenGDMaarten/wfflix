<?php

class CourseModel
{
    protected $conn;

    /* Gets the database connection + database access information as provided in the root index.php file */
    public function __construct()
    {
        $this->conn = App::get('query');
    }

    /* Grabs all data from the Courses table and prepares it for the Controller */
    public function showAll()
    {
        $sql = "SELECT * FROM courses";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    /* Shows all Courses for Teacher Dashboard. Only shows courses created by that specific teacher. */
    public function showAllTeacher()
    {
        $sql = "SELECT * FROM courses WHERE User_id = {$_SESSION["userid"]}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    /* Creates a course */
    public function add()
    {
        $sql = "INSERT INTO courses (course, course_title, course_description, Category_id, author, img, vid, vid_2, exercise, 
                     exercise_2, associated_course, associated_course_2, associated_course_3, hours, User_id)
                VALUES('{$_POST['courseName']}','{$_POST['courseTitel']}','{$_POST['courseDescription']}', '{$_POST['Category_id']}',
                       '{$_POST['courseAuthor']}','{$_POST['courseImg']}','{$_POST['courseVid']}','{$_POST['courseVid_two']}',
                       '{$_POST['courseExercise']}','{$_POST['courseExercise_two']}','{$_POST['courseAssociated']}', '{$_POST['courseAssociated_2']}',
                       '{$_POST['courseExercise_two']}', '{$_POST['courseTime']}', '{$_POST['useridcourse']}')";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    /* Deletes a course */
    public function delete()
    {
        // Hard Delete
        $sql = "DELETE FROM courses WHERE Course_id
                              = {$_POST['id']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    /* Grabs data from database to put in the update course page */
    public function update()
    {
        $sql = "SELECT * FROM courses WHERE Course_id
                                = {$_POST['id']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    /* Updates Course Data */
    public function store()
    {
        // If User is an Admin -> Don't update User_id, value is used in TeacherDashboard.
        if ($_SESSION["User_type_id"] == 1) {

            $sql = "UPDATE courses SET 
                    course = '{$_POST['courseName']}',
                    course_title = '{$_POST['courseTitel']}',
                    course_description =  '{$_POST['courseDescription']}',
                    Category_id =  '{$_POST['Category_id']}',
                    author = '{$_POST['courseAuthor']}',
                    img = '{$_POST['courseImg']}',
                    vid =  '{$_POST['courseVid']}',
                    vid_2 =  '{$_POST['courseVid_two']}',
                    exercise =  '{$_POST['courseExercise']}',
                    exercise_2 =   '{$_POST['courseExercise_two']}',
                    associated_course =  '{$_POST['courseAssociated']}',
                    associated_course_2 =    '{$_POST['courseAssociated_2']}',
                    associated_course_3 =    '{$_POST['courseExercise_two']}',
                    hours =    '{$_POST['courseTime']}',

                    updated_at = current_timestamp WHERE Course_id
                                                             = {$_POST['courseid']}";

        } else {
            $sql = "UPDATE courses SET 
                    course = '{$_POST['courseName']}',
                    course_title = '{$_POST['courseTitel']}',
                    course_description =  '{$_POST['courseDescription']}',
                    Category_id =  '{$_POST['Category_id']}',
                    author = '{$_POST['courseAuthor']}',
                    img = '{$_POST['courseImg']}',
                    vid =  '{$_POST['courseVid']}',
                    vid_2 =  '{$_POST['courseVid_two']}',
                    exercise =  '{$_POST['courseExercise']}',
                    exercise_2 =   '{$_POST['courseExercise_two']}',
                    associated_course =  '{$_POST['courseAssociated']}',
                    associated_course_2 =    '{$_POST['courseAssociated_2']}',
                    associated_course_3 =    '{$_POST['courseExercise_two']}',
                    hours =    '{$_POST['courseTime']}',
                    User_id =    '{$_POST['useridcourse']}',

                    updated_at = current_timestamp WHERE Course_id
                                                             = {$_POST['courseid']}";
        }

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

    }

    /* Loads a specific course data on Course_Specific depending on which course was pressed on Course_overview */
    public function details()
    {
        $sql = "SELECT * FROM courses WHERE Course_id = {$_POST['id']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    /* Grabs data from the database which is filtered depending on which button uses this function */
    public function filter()
    {
        $sql = "SELECT * FROM courses WHERE {$_POST['ColumnName']} = {$_POST['RowValue']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    /* Grabs data from the database which is filtered depending on which button uses this function */
    public function filterTeacher()
    {
        $sql = "SELECT * FROM courses WHERE {$_POST['ColumnName']} = {$_POST['RowValue']} AND User_id = {$_SESSION["userid"]}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    /* Grabs data from the database which corresponds to a value in the keyword column */
    public function filterSearch()
    {
        /* Makes sure any value searched can be used by the database */
        $searchconcat = "'%" . $_POST['RowValue'] . "%'";

        $sql = "SELECT * FROM courses WHERE {$_POST['ColumnName']} LIKE $searchconcat";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

}

?>