<?php

class UserModel
{
    /* Protects the DB connection */
    protected $conn;

    /* Creates a DB connection through the App Class */
    public function __construct()
    {
        $this->conn = App::get('query');
    }

    /* Grabs all user data from the users table */
    public function showAll()
    {
        $sql = "SELECT * FROM users";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    /* Grabs all user softdeleted data from the users table */
    public function showAllDel()
    {
        $sql = "SELECT * FROM users
         WHERE enable = 0";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    /* Creates a new row (with the provided data) in the users table */
    public function add()
    {
        $hash = password_hash($_POST['password'], PASSWORD_BCRYPT);

        $sql = "INSERT INTO users (username, password, email, User_type_id)
                VALUES('{$_POST['userName']}', '$hash', '{$_POST['email']}', '{$_POST['userTypeId']}')";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    /* Deletes a specified row from the users table */
    public function delete()
    {
        /* Hard deletes */
        $sql = "DELETE FROM users WHERE User_id = {$_POST['userid']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    /* Soft deletes a specified row from the users table */
    public function softDelete()
    {
        $sql = "UPDATE users SET 
                    enable = '{$_POST['enable']}',
                    updated_at = current_timestamp 
               
                    WHERE User_id = {$_POST['userid']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    /* Grabs all user data that correspondents to the provided User_id */
    public function update()
    {
        $sql = "SELECT * FROM users WHERE User_id = {$_POST['userid']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    /* Grabs all user data that correspondents to the provided User_id, this can be merged with the above method (if I have time) */
    public function updatePass()
    {
        $sql = "SELECT * FROM users WHERE User_id = {$_POST['userid']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    /* Updates the provided data that correspondents to a row with the provided User_id */
    public function store()
    {
        $sql = "UPDATE users SET 
                    username = '{$_POST['userName']}', 
                    email = '{$_POST['email']}',     
                    updated_at = current_timestamp WHERE User_id = {$_POST['userid']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    /* Updates the provided data that correspondents to a row with the provided User_id,
    this can be merged with an if statement with the above method (if I have time)  */
    public function storeAdmin()
    {
        $sql = "UPDATE users SET 
                    username = '{$_POST['userName']}', 
                    User_type_id = '{$_POST['userTypeId']}', 
                    email = '{$_POST['email']}',     
                    updated_at = current_timestamp WHERE User_id = {$_POST['userid']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    /* Grabs all user data that correspondents to the provided SESSION_User_id */
    public function updateProfile()
    {
        $sql = "SELECT * FROM users WHERE User_id = {$_SESSION['userid']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    /* Updates a password and salts it */
    public function storePass()
    {
        $hash = password_hash($_POST['password'], PASSWORD_BCRYPT);

        $sql = "UPDATE users SET 
                    password = '$hash',  
                    updated_at = current_timestamp 
             WHERE User_id = {$_POST['userid']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    /* Updates the Git_Done depending on the gitDone value provided (most likely the INT(1)) */
    public function gitDone()
    {
        $sql = "UPDATE users SET 
                    git_done = '{$_POST['gitDone']}',     
                    updated_at = current_timestamp WHERE User_id = {$_POST['userid']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

}
?>
