<?php

/* I'll put the location(s) where/how the route is used in a comment behind the route \\ Haven't had the time to finish it. */

/* ---------------------- */
/* ----- Get routes ----- */
/* ---------------------- */

    //PagesController
$router->get('', 'PagesController@home'); /* Navbar // Anytime a function requires a root callback */
$router->get('over-ons', 'PagesController@about'); /* Navbar */
$router->get('contact', 'PagesController@contact'); /* Navbar */
$router->get('not_found', 'PagesController@notFound'); /* Triggers if no search result is found @ course_overview */
$router->get('tutorial', 'PagesController@tutorial'); /* Login Page */

    //AccountController
$router->get('user', 'AccountController@index');
$router->get('login', 'AccountController@login'); /* NavBar */
$router->get('welcome', 'AccountController@welcome'); /* Redirect after logging in */
$router->get('logout', 'AccountController@logout'); /* Logout button pressed */
$router->get('reset', 'AccountController@reset');
$router->get('resetrequest', 'AccountController@resetrequest');
$router->get('registreer', 'AccountController@registreer');
$router->get('forgot-pass', 'AccountController@forgotPass');


    //CourseController
$router->get('course', 'CourseController@index');
$router->get('courseoverzicht', 'CourseController@home');
$router->get('coursepagina', 'CourseController@details'); /*course_overzicht.view*/
$router->get('geen_match', 'CourseController@geen_match'); /*course_overzicht.view*/

    //AdminController
$router->get('admin', 'AdminController@home');
$router->get('courses', 'AdminController@courses');
$router->get('gebruikers', 'AdminController@gebruikers');
$router->get('help', 'AdminController@help');
$router->get('admin-add-course', 'AdminController@addcourse');
$router->get('admin-add-user', 'AdminController@addUser');

    //TeacherController
$router->get('teacher', 'TeacherController@home');
$router->get('teacher_courses', 'TeacherController@courses');
$router->get('teacher_help', 'TeacherController@help');
$router->get('teacher-add-course', 'TeacherController@addcourse');


/* ----------------------- */
/* ----- Post routes ----- */
/* ----------------------- */
    //PagesController
$router->post('contact', 'PagesController@contact'); /* Contact Form */


    //AccountController
$router->post('login', 'AccountController@login');
$router->post('welcome', 'AccountController@welcome');
$router->post('logout', 'AccountController@logout');
$router->post('reset', 'AccountController@reset');
$router->post('registreer', 'AccountController@registreer');
$router->post('forgot-pass', 'AccountController@forgotPass');
$router->post('resetrequest', 'AccountController@resetrequest');

    //CourseController
$router->post('add-course', 'CourseController@add');
$router->post('teacher-add-course', 'CourseController@addTeacher');
$router->post('del-course', 'CourseController@delete');
$router->post('upd-course', 'CourseController@update');
$router->post('teacher-del-course', 'CourseController@deleteTeacher');
$router->post('teacher-upd-course', 'CourseController@updateTeacher');
$router->post('store-course', 'CourseController@store');
$router->post('courseoverzicht', 'CourseController@home'); // Used - /*course_overzicht.view*/ /*no_search_match.view*/
$router->post('coursepagina', 'CourseController@details'); // Used -- /*course_overzicht.view*/
$router->post('courses', 'CourseController@filterAdmin'); // Used -- /*admin_course.view*/
$router->post('coursesfilter', 'CourseController@filterUser'); // Used -- /*course_overview.view*/
$router->post('coursesteacher', 'CourseController@filterTeacher'); // Used -- /*admin_course.view*/
$router->post('coursessearch', 'CourseController@filterSearch'); // Used -- /*course_overzicht.view*/
$router->post('courseoverzicht-search', 'CourseController@filterSearchUser'); // Used -- /*course_overzicht.view*/

    //UserController
$router->post('add-user', 'UserController@add');
$router->post('del-user', 'UserController@delete');
$router->post('upd-user', 'UserController@update');
$router->post('upd-userPass', 'UserController@updatePass');
$router->post('store-userAdmin', 'UserController@storeAdmin');
$router->post('store-user', 'UserController@store');
$router->post('store-userPass', 'UserController@storePass');
$router->post('git-done', 'UserController@gitDone');
$router->post('softdel-user', 'UserController@softDelete');

    //AdminController
$router->post('coursesreset', 'AdminController@courses'); // Used -- /*admin_course.view*/

    //TeacherController
$router->post('coursesreset-teacher', 'TeacherController@courses'); // Used -- /*admin_course.view*/
?>