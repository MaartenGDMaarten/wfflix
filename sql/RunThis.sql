create table wfflix.contact_form
(
    id         int auto_increment
        primary key,
    first_name varchar(63)  not null,
    last_name  varchar(63)  not null,
    email      varchar(127) not null,
    subject    varchar(63)  not null,
    category   varchar(127) null,
    comment    text         null,
    constraint category
        check (`category` in ('Course', 'Verzending', 'Betaling', 'Klacht', 'Overig'))
);

create table wfflix.courses
(
    Course_id           int(8) auto_increment
        primary key,
    course              varchar(50)                 null,
    enable              int       default 1         null,
    Category_id         int                         null,
    User_id             int                         null,
    course_title        varchar(128)                null,
    course_description  text                        null,
    author              varchar(256)                null,
    extra_description   text                        null,
    keyword             varchar(100)                null,
    img                 text                        null,
    img_2               text                        null,
    img_3               text                        null,
    img_4               text                        null,
    img_5               text                        null,
    img_6               text                        null,
    created_at          timestamp                   null,
    updated_at          timestamp default curdate() not null,
    deleted_at          timestamp                   null,
    note                text                        null,
    vid                 text                        null,
    vid_2               text                        null,
    exercise            text                        null,
    exercise_2          text                        null,
    associated_course   varchar(64)                 null,
    associated_course_2 varchar(64)                 null,
    associated_course_3 varchar(64)                 null,
    hours               time                        null,
    exercise_amounts    int                         null,
    video_amount        int                         null,
    lesson_amount       int                         null
);

create table wfflix.password_reset_request
(
    id             int unsigned auto_increment
        primary key,
    user_id        int unsigned not null,
    date_requested datetime     not null,
    token          varchar(255) not null
)
    collate = utf8mb3_unicode_ci;

create table wfflix.user_types
(
    User_type_id int          not null
        primary key,
    name         varchar(15)  null,
    description  varchar(255) null,
    modified_at  timestamp    null
);

create table wfflix.users
(
    User_id              int auto_increment
        primary key,
    User_type_id         int                                  not null,
    username             varchar(63)                          not null,
    password             varchar(127)                         not null,
    email                varchar(127)                         null,
    last_login           timestamp                            null,
    last_password_change timestamp                            null,
    created_at           datetime default current_timestamp() null,
    updated_at           datetime default current_timestamp() not null,
    deleted_at           timestamp                            null,
    git_done             int      default 0                   null,
    enable               int      default 1                   null,
    constraint email
        unique (email),
    constraint username
        unique (username)
);

create table wfflix.admins
(
    Admin_id    int auto_increment
        primary key,
    User_id     int       not null,
    modified_at timestamp null,
    constraint admins_ibfk_1
        foreign key (User_id) references wfflix.users (User_id)
            on delete cascade
);

create index User_id
    on wfflix.admins (User_id);

create index User_type_id
    on wfflix.users (User_type_id);

insert into wfflix.courses (Course_id, course, enable, Category_id, User_id, course_title, course_description, author, extra_description, keyword, img, img_2, img_3, img_4, img_5, img_6, created_at, updated_at, deleted_at, note, vid, vid_2, exercise, exercise_2, associated_course, associated_course_2, associated_course_3, hours, exercise_amounts, video_amount, lesson_amount)
values  (1, 'GIT', 1, 1, 2, 'GIT 101', 'Leer hier alles over Git, GitLab, GitHub, GitGud', 'Stephan', 'Git is een VCS waarmee je snel en gemakkelijk gegevens kan delen.', 'Git, GitLab, GitHub, VCS', '/images/courses/git.png', null, null, null, null, null, null, '2022-11-03 00:00:00', null, null, 'https://www.youtube.com/embed/Tvw_k-nBLTQ', null, 'Yep', null, 'HTML', 'CSS', 'PHP', '03:00:00', null, null, null),
        (2, 'php', 1, 3, 2, 'Php Today', 'learn about what the basics of php will bring you', 'Jeffrey', 'Bring your project to life today!', 'Jeffrey, php, back-end, basics, website', '/images/courses/php.png', null, null, null, null, null, null, '2022-11-03 00:00:00', null, null, null, null, null, null, 'HTML', 'CSS', 'PHP', '03:00:00', null, null, null),
        (3, 'sql', 1, 3, 2, 'Sql Today', 'learn about what the basics of sql will bring you', 'Jeffrey', 'Bring your project to life today!', 'Jeffrey, sql, back-end, basics, database', '/images/courses/sql1.png', null, null, null, null, null, null, '2022-11-03 00:00:00', null, null, null, null, null, null, 'HTML', 'CSS', 'PHP', '03:00:00', null, null, null),
        (4, 'html', 1, 2, 2, 'Html Today', 'learn about what the basics of html will bring you', 'Fiasal', 'Bring your project to life today!', 'Fiasal, html, front-end, basics, website', '/images/courses/html1.png', null, null, null, null, null, null, '2022-11-03 00:00:00', null, null, null, null, null, null, 'HTML', 'CSS', 'PHP', '03:00:00', null, null, null),
        (5, 'html2', 1, 2, 2, 'Html2 Today', 'learn about what the basics of html will bring you', 'Tobie', 'Bring your project to life today!', 'Tobie, html, back-end, basics, website', '/images/courses/html2.png', null, null, null, null, null, null, '2022-11-03 00:00:00', null, null, null, null, null, null, 'HTML', 'CSS', 'PHP', '03:00:00', null, null, null),
        (6, 'css', 1, 2, 2, 'Css Today', 'learn about what the basics of css will bring you', 'Fiasal', 'Bring your project to life today!', 'Fiasal, css, front-end, basics, website', '/images/courses/css1.png', null, null, null, null, null, null, '2022-11-03 00:00:00', null, null, null, null, null, null, 'HTML', 'CSS', 'PHP', '03:00:00', null, null, null),
        (7, 'javascript', 1, 2, 2, 'Javascript Today', 'learn about what the basics of javascript will bring you', 'Tobie', 'Bring your project to life today!', 'Tobie, javascript, js, front-end, website, interactive', '/images/courses/js1.png', null, null, null, null, null, null, '2022-11-03 00:00:00', null, null, 'https://www.youtube.com/watch?v=Mr1hus6uSlU&list=PLavMXd9y3NVAGzbWtfs_kE9NXPdG5eZ37&index=6&ab_channel=StephanHoeksema', null, 'yep', null, 'HTML', 'CSS', 'PHP', '03:00:00', null, null, null),
        (8, 'C#', 1, 3, 2, 'C# Today', 'learn about what the basics of C# will bring you', 'Maarten', 'Bring your project to life today!', 'Maarten, c#, front-end, basics, gamedevelepment, game', '/images/courses/c1.png', null, null, null, null, null, null, '2022-11-03 00:00:00', null, null, null, null, null, null, 'HTML', 'CSS', 'PHP', '03:00:00', null, null, null),
        (9, 'C++', 1, 3, 2, 'C++ Today', 'learn about what the basics of C++ will bring you', 'Maarten', 'Bring your project to life today!', 'Maarten, C++, front-end, basics, notepad', '/images/courses/c_c1.png', null, null, null, null, null, null, '2022-11-03 00:00:00', null, null, null, null, null, null, 'HTML', 'CSS', 'PHP', '03:00:00', null, null, null);

insert into wfflix.password_reset_request (id, user_id, date_requested, token)
values  (1, 43, '2022-10-21 20:11:27', 'd5d65f283c462a443a70036de1dbb4f6'),
        (2, 43, '2022-10-21 20:17:54', 'e901e6f433bb0aa2cd904175e38812a4'),
        (3, 43, '2022-10-21 20:29:18', 'c3af30ef2f78a19df0fde57f38083ec4'),
        (4, 43, '2022-10-21 20:36:07', '339d6196f876dc1d700c427c1f779709'),
        (5, 43, '2022-10-21 20:37:42', '58e5f82687be03b96143269c544254f1'),
        (6, 15, '2022-10-21 20:55:04', '49c51da64ec5d1d3c0bdbe6b5f52be4c'),
        (7, 43, '2022-10-21 20:55:12', '8d593997fb1b05debdf802645ca24bb9'),
        (8, 15, '2022-10-21 20:56:08', '3911faf06033c9940436a245f6e77ecd'),
        (9, 43, '2022-10-21 20:56:12', 'c776e4e5f73f3bdcbda9a13c5ac69c66'),
        (10, 43, '2022-10-21 20:57:58', '2e307a5a9654cda15f082fc08866bdfe'),
        (11, 43, '2022-10-21 21:11:19', '77deeb3e155ed2e48ea77aac65e61d2b'),
        (12, 43, '2022-10-21 21:15:42', '1dd08c3d00ce31df45fd218470ac3d6b'),
        (13, 43, '2022-10-21 21:15:46', 'f3d65e1df32e65b3ba247c7502c7359c'),
        (14, 44, '2022-10-21 21:18:34', '3b9b14ad3f56aeb6c1c9fee837f3fc9b'),
        (15, 44, '2022-10-21 21:24:44', '03143de63326c0756a03b6fdd7751430'),
        (16, 44, '2022-10-21 21:27:29', '6e916eff68c17529ddc9454d28f34583'),
        (17, 44, '2022-10-21 21:27:54', '6ea68014f22b1dc17807bce10a4c6df1'),
        (18, 44, '2022-10-21 21:31:09', '7e9b7b49061e9ca45f62a95feadc700a'),
        (19, 45, '2022-10-21 21:32:59', '91ed66ea0d08337df68a86105a3cef0d'),
        (20, 45, '2022-10-21 21:37:36', '260e4804d293cfa99e3ddeb03e40964b'),
        (21, 45, '2022-10-21 21:42:53', 'd862e55ae4d1dd1e83d5025f9da78b30'),
        (22, 45, '2022-10-21 21:48:18', '528501cfd5acee94fd3c6286a1b6f319'),
        (23, 15, '2022-10-21 21:50:50', '8ba8602e082e53ddc1c6a35f256b8bb9'),
        (24, 15, '2022-10-22 17:10:42', '90ee88f1c65dfd76fd3a4f37788e812f'),
        (25, 15, '2022-10-22 17:10:44', 'c97aa9432adf998f78d45e7d480ee616'),
        (26, 15, '2022-10-22 17:10:46', '01692c22fd997fcb4e20780dff67cbd5'),
        (27, 15, '2022-10-22 17:10:54', '8ecc513e4978c414626c2c87007cf294'),
        (28, 15, '2022-10-22 17:12:09', '84c7d1a6480a8c57e7fa7d528a0d4e27'),
        (29, 4, '2022-10-29 17:20:11', 'eea030d574c64bbe15948a7579b106c5'),
        (30, 1, '2022-10-30 13:43:05', '70086fadc446ffe1b66b13317e2e2cb3'),
        (31, 1, '2022-10-31 10:16:55', '535ca418749a04d51264c2b48fc78282'),
        (32, 1, '2022-11-02 17:23:35', 'be65b90867e4587c7efa9726e2d8fd44'),
        (33, 1, '2022-11-02 17:28:53', 'e410612417ec36190181c5559da9fe81'),
        (34, 1, '2022-11-03 13:14:14', 'c4f331a212a26ce7be6386cee811d723');


insert into wfflix.users (User_id, User_type_id, username, password, email, last_login, last_password_change, created_at, updated_at, deleted_at, git_done, enable)
values  (1, 1, 'Admin', '$2y$10$3F0K8zjH.DxRlnUURoKIWOBrr.i5Vj0iHUTY7GVWCbvtY92Oln7C6', 'Admin@gmail.com', null, null, '2022-10-29 19:17:18', '2022-10-29 19:17:18', null, 0, 1),
        (2, 3, 'Teacher', '$2y$10$QTTtHs4G5UvCo/7J1GNGY.QEZ9K1peMJ0Pz0n1Ub033dPkLB7c5D.', 'Teacher@gmail.com', null, null, '2022-10-29 19:17:18', '2022-11-02 10:02:02', null, 0, 1),
        (3, 2, 'Student', '$2y$10$AYkHIR7X2FXA0pgpz9.RZ.uSRzp5q7bmWd2dsNfexeub8P/ADw.Ni', 'Student@gmail.com', null, null, '2022-10-29 19:17:18', '2022-11-02 18:02:15', null, 0, 1);