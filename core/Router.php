<?php
/* Used in Index.php */

/* All routes go through this class */
class Router {

    /* Protects the routes */
    protected $routes = [];

    /* Makes the connection to the routes file */
    public static function load($file) {
        $router = new static; //$router = new Router;
        require $file;
        return $router;
    }

    /* Prepares the request for the methods below */
    public function define($routes) {
        $this->routes = $routes;
    }

    /* Check if the route is a GET */
    public function get($uri, $controller) {
        $this->routes['GET'][$uri] = $controller;
    }

    /* Check if the route is a POST */
    public function post($uri, $controller) {
        $this->routes['POST'][$uri] = $controller;
    }

    /* Method Direct -
    Checks if the routes does exist by splitting the requested route around the '@'
    LeftSide of the @ = the controller(Class)
    RightSide of the @ = the method within the controller
    */
    public function direct($uri, $requestType) {
        if(array_key_exists($uri, $this->routes[$requestType])){
            return $this->callAction(...explode('@', $this->routes[$requestType][$uri]));
        }

        /* If no route is found */
        else {
            /* For testing purposes/error finding */
//            throw new Exception('Route not defined');

            /* Used to load the 404 not found page if no route is found */
            header("Location: not_found");
        }
    }

    /* If the route does exist but the corresponding METHOD within the controller does NOT, it will throw an error */
    protected function callAction($controller, $action) {
        $controller = new $controller;
        if(!method_exists($controller, $action)) {
            throw new Exception('Method does not exist.');
        }
        /* Otherwise it will return the requested data */
        return $controller->$action();
    }
}








