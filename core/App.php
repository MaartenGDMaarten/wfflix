<?php
/* Used in index.php */
// This is a Dependency Injection

class App {
    protected static $box = [];

    /* Make a key (config) with a value (config.php) accessible */
    public static function bind($key, $value) {
        static::$box[$key] = $value;
    }

    /* Retrieves the created key (which also has a value in it) */
    public static function get($key) {
        return static::$box[$key];
    }
}