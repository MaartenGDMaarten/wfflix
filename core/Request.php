<?php
/* Used in Index.php */

/* Used in combination with the Router Class */
class Request
{
    /* Trims and prepares the URI for the Router Class */
    public static function uri()
    {
        return trim($_SERVER['REQUEST_URI'], '/');
    }

    /* Prepares the Request Method for the Router Class */
    public static function method() {
        return $_SERVER['REQUEST_METHOD'];
    }

}