<?php
/* Used in Index.php */


/* Adds the functionality to call upon a view by only naming the starting name of a file */
function view($name, $data = []) {
    extract($data);
    return require "views/{$name}.view.php";


}