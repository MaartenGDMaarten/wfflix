<?php

class PagesController {

    /* Loads homepage */
    public function home() {
        return view('index');
    }

    /* Loads about us page */
    public function about() {
        return view('about');
    }

    /* Loads contact page */
    public function contact() {
        return view('contact');
    }

    /* Loads not found page */
    public function notFound() {
        return view('not_found');
    }

    /* Loads not found page */
    public function tutorial() {
        return view('tutorial');
    }

}