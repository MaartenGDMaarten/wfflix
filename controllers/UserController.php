<?php

class UserController
{

    /* Created a dummy/copy of UserModel, so you can call upon its methods */
    public function __construct()
    {
        $this->user = new UserModel();
    }

    /* Loads the Users variable with all user database data */
    public function home()
    {
        $users = $this->user->showAll();

        return view('user', compact('users'));
    }

    /* Redirect admin back to admin dashboard after creating a user */
    public function add()
    {
        $this->user->add();

        header("Location: gebruikers");
    }

    /* Redirect admin back to admin dashboard after deleting a user - including security */
    public function delete()
    {
        $this->user->delete();

        if (@$_SESSION["User_type_id"] <> 1) {
            header("Location: /");
        } else {
            header("Location: gebruikers");
        }
    }

    /*  Redirect admin back to admin dashboard after soft deleting a user */
    public function softDelete()
    {
        $this->user->softDelete();

        header("Location: gebruikers");
    }



    /* Loads edit user on the admin dashboard with the user data loaded */
    public function update()
    {
        $user = $this->user->update();

        return view('admin/user-upd', compact('user'));
    }

    /* Loads edit user password on the admin dashboard with the user data loaded */
    public function updatePass()
    {
        $user = $this->user->updatePass();

        return view('admin/user-updpass', compact('user'));
    }

    /* Redirect a user to the welcome page after updating an account */
    public function store()
    {
        $this->user->store();

        header("Location: welcome");
    }

    /* Redirect an admin to the admin dashboard users page after updating an account - including security */
    public function storeAdmin()
    {
        $this->user->storeAdmin();

        if (@$_SESSION["User_type_id"] <> 1) {
            header("Location: /");
        } else {
            header("Location: gebruikers");
        }

    }

    /* Redirect an admin to the admin dashboard users page after updating a password*/
    public function storePass()
    {
        $this->user->storePass();

        header("Location: gebruikers");
    }

    /* Updates the git_done value and redirects back to the courses page */
    public function gitDone()
    {
        $this->user->gitDone();

        $_SESSION["git_done"] = $_POST['gitDone'];

        header("Location: courseoverzicht");
    }


}