<?php

class TeacherController
{

    /* Created a dummy/copy of CourseModel so you can call upon its methods */
    public function __construct() {
        $this->course = new CourseModel();
    }

    /* Loads teacher dashboard homepage */
    public function home()
    {
        $courses = $this->course->showAllTeacher();
        return view('teacher/teacher', compact('courses'));
    }

    /* Loads teacher dashboard courses */
    public function courses()
    {
        $courses = $this->course->showAllTeacher();
        return view('teacher/teacher_course', compact('courses'));
    }

    /* Loads teacher dashboard add course page */
    public function addcourse()
    {
        return view('teacher/teacher-course-add');
    }

    /* Loads teacher dashboard help page */
    public function help()
    {
        $courses = $this->course->showAllTeacher();
        return view('teacher/teacher_help', compact('courses'));
    }


}

?>