<?php

class AdminController
{

    /* Created a dummy/copy of CourseModel so you can call upon its methods */
    /* Created a dummy/copy of UserModel so you can call upon its methods */
    public function __construct() {
        $this->course = new CourseModel();
        $this->user = new UserModel();
        $this->userDel = new UserModel();
    }

    /* Opens admin mainpage with loaded variables */
    public function home()
    {
        $courses = $this->course->showAll();
        $users = $this->user->showAll();
        $usersDel = $this->userDel->showAllDel();
        return view('admin/admin', compact('courses', ('users')));
    }

    /* Opens admin courses with loaded variables */
    public function courses()
    {
        $courses = $this->course->showAll();
        $users = $this->user->showAll();
        return view('admin/admin_course', compact('courses', ('users')));
    }

    /* Opens admin add course page */
    public function addcourse()
    {
        return view('admin/course-add');
    }

    /* Opens admin users with loaded variables */
    public function gebruikers()
    {
        $courses = $this->course->showAll();
        $users = $this->user->showAll();
        $usersDel = $this->userDel->showAllDel();
        return view('admin/admin_user', compact('courses', 'usersDel', ('users')));
    }

    /* Opens admin add user page */
    public function addUser()
    {
        return view('admin/user-add');
    }

    /* Opens admin help with loaded variables */
    public function help()
    {
        $courses = $this->course->showAll();
        $users = $this->user->showAll();
        return view('admin/admin_help', compact('courses', ('users')));
    }






}

?>