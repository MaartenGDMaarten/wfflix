<?php

class CourseController {

    /* Creates a "dummy/copy" of the CourseModel, so you can run the methods within CourseModel in the CourseController */
    public function __construct() {
        $this->course = new CourseModel();
    }

    /* Makes the $courses variable available on Course_overview, has all courses table data within it */
    public function home() {

        $courses = $this->course->showAll();

        return view('course_overview', compact('courses'));
    }

    /* Redirects to the No_match page als je helemaal geen courses krijgt met een search */
    public function geen_match() {

        return view('no_search_match');
    }

    /* Creates a course with Admin redirect */
    public function add()
    {

        $this->course->add();

        header("Location: courses");
    }

    /* Creates a course with Teacher redirect */
    public function addTeacher()
    {

        $this->course->add();

        header("Location: teacher_courses");
    }

    /* Deletes a course with admin redirect & security */
    public function delete()
    {
        $this->course->delete();

        if (@$_SESSION["User_type_id"] <> 1) {
            header("Location: courseoverzicht");
        }
        else{
            header("Location: courses");
        }
    }

    /* Deletes a course with teacher redirect & security */
    public function deleteTeacher()
    {
        $this->course->delete();

        if (@$_SESSION["User_type_id"] <> 3) {
            header("Location: courseoverzicht");
        }
        else{
            header("Location: teacher_courses");
        }
    }

    /* Puts data grabbed in the CourseModel update method and displays it on the course-upd page */
    public function update()
    {
        $course = $this->course->update();

        return view('admin/course-upd', compact('course'));
    }

    public function updateTeacher()
    {
        $course = $this->course->update();

        return view('teacher/teacher-course-upd', compact('course'));
    }

    /*  Redirect admin or teacher after database update  */
    public function store()
    {
        $this->course->store();

        if (@$_SESSION["User_type_id"] == 1) {
            header("Location: courses");
        }
        elseif (@$_SESSION["User_type_id"] == 3) {
            header("Location: teacher_courses");
        }
        else{
            header("Location: courseoverzicht");
        }
    }

    /*  Displays the details method data in the Course_specific page  */
    public function details()
    {
        $course = $this->course->details();

        return view('course_specific', compact('course'));
    }

    /*  Redirect admin after filter is applied */
    public function filterAdmin()
    {
        $coursesF = $this->course->filter();

        return view('admin/admin_course', compact('coursesF'));
    }

    /*  Redirect teacher after filter is applied */
    public function filterTeacher()
    {
        $coursesF = $this->course->filterTeacher();

        return view('teacher/teacher_course', compact('coursesF'));
    }

    /*  Redirect user after filter is applied */
    public function filterUser()
    {
        $coursesF = $this->course->filter();

        return view('course_overview', compact('coursesF'));
    }

    /* Applies filtered search data to the admin course page */
    public function filterSearch()
    {
        $coursesF = $this->course->filterSearch();

        if (@$_SESSION["User_type_id"] == 1) {
            return view('admin/admin_course', compact('coursesF'));
        }
        else {
            return view('teacher/teacher_course', compact('coursesF'));
        }
    }

    /* Applies filtered search data to the teacher course page */
    public function filterSearchUser()
    {
        $coursesF = $this->course->filterSearch();

        return view('course_overview', compact('coursesF'));
    }


}

?>