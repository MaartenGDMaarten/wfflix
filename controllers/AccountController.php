<?php

class AccountController
{

    /* Creates an usable dummy/copy of UserModel so you can use its methods */
    public function __construct()
    {
        $this->user = new UserModel();
    }

    /* Opens User Home Page */
    public function home()
    {
        $users = $this->user->showAll();

        return view('user', compact('users'));
    }

    /* Redirects to Admin User Dashboard after creating an account via dashboard. */
    public function add()
    {
        $this->user->add();

        header("Location: gebruikers");   //REP
    }

    /* Redirects to Admin User Dashboard or index after deleting an account (depending on the User_type state) */
    public function delete()
    {
        $this->user->delete();

        if (@$_SESSION["User_type_id"] <> 1) {
            header("Location: /");
        } else {
            header("Location: gebruikers");
        }
    }

    /* Loads admin specific user update page */
    public function update()
    {
        $user = $this->user->update();

        return view('admin/user-upd', compact('user'));
    }

    /* Redirects to Admin User Dashboard after updating an account as admin */
    public function store()
    {
        $this->user->store();

        if (@$_SESSION["User_type_id"] <> 1) {
            header("Location: /");
        } else {
            header("Location: gebruikers");
        }

    }

    /* Loads login page */
    public function login()
    {
        require 'views/account/login.php';
    }

    /* Loads register page */
    public function registreer()
    {
        require 'views/account/register.php';
    }

    /* Loads forgot password page */
    public function forgotPass()
    {
        require 'views/account/forgot_password.php';
    }

    /* Loads welcome page with loaded user data */
    public function welcome()
    {
        $user = $this->user->updateProfile();

        require 'views/account/welcome.php';
    }

    /* Loads logout page */
    public function logout()
    {
        require 'views/account/logout.php';
    }

    /* Loads password reset page */
    public function reset()
    {
        require 'views/account/passwordreset.php';
    }

    /* Loads forgot_password page */
    public function resetrequest()
    {
        require 'views/account/forgot_password_reset.php';
    }

}
?>