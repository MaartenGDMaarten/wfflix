<?php
/* Creates a database connection and loads the required credentials ($config variable) */
class Connection
{
    public static function make($config){
        $connection = $config['database']['connection'];
        $database = $config['database']['name'];
        $username = $config['database']['username'];
        $password = $config['database']['password'];
        $options = $config['database']['options'];

        try {
            $conn = new PDO("mysql:host=$connection;dbname=$database", $username, $password, $options );
        } catch( PDOException $e ) {
            echo 'Connection failed: ' . $e->getMessage();
            die;
        }

        return $conn;
    }
}