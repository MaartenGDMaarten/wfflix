<!-- Adds the credentials to access the database -->

<?php

return [
    'database' => [
        'name' => 'wfflix',
        'username' => 'root',
        'password' => '',
        'connection' => '127.0.0.1',
        'options' => [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ]
    ]
];